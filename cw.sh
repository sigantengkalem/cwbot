#!/bin/bash

nep="channel#1360378941"
main="🐉[SHU]riken"
#quest[0]="⛰️Valley"
quest[0]="🌋Devil's Valley"
#quest[1]="🌲Forest"
quest[1]="🧟‍♀️Dead Marshes"
#quest[2]="🍄Swamp"
quest[2]="🍂Mirkwood"
quest[3]="🗡Foray"
serang[0]="🗡Head"
serang[1]="🗡Body"
serang[2]="🗡Feet"
defend[0]="🛡Head"
defend[1]="🛡Body"
defend[2]="🛡Feet"
status=""
cwBot="user#408101137" #don't change this
# create a textfile called "log" with you telegram log file
log=$(<log) 
touch lastStaminaRestored.txt lastStaminaStat.txt lastStop.txt lastLevelUp.txt lastGo.txt lastRec.txt lastPillage.txt lastBeat.txt lastWin.txt lastBattle.txt  lastDire.txt lastDefeated.txt lastDice.txt lastFight.txt lastDef.txt lastVS.txt lastAtk.txt lastArena.txt lastWind.txt lastResult.txt lastBold.txt lastLevelUp2.txt lastStrike.txt lastSwamp.txt lastMountain.txt lastEngage.txt lastStock.txt lastHide.txt lastValley.txt
hiding=0
lastEngage=$(<lastEngage.txt)
lastSwamp=$(<lastSwamp.txt)
lastHide=$(<lastHide.txt)
lastStock=$(<lastStock.txt)
lastMountain=$(<lastMountain.txt)
lastValley=$(<lastValley.txt)
lastStaminaRestored=$(<lastStaminaRestored.txt)
lastStaminaStat=$(<lastStaminaStat.txt)
lastLevelUp=$(<lastLevelUp.txt)
lastLevelUp2=$(<lastLevelUp2.txt)
lastGo=$(<lastGo.txt)
lastRec=$(<lastRec.txt)
lastStop=$(<lastStop.txt)
lastPillage=$(<lastPillage.txt)
lastBeat=$(<lastBeat.txt)
lastWin=$(<lastWin.txt)
lastBattle=$(<lastBattle.txt)
lastDire=$(<lastDire.txt)
lastDefeated=$(<lastDefeated.txt)
lastDice=$(<lastDice.txt)
lastFight=$(<lastFight.txt)
lastDef=$(<lastDef.txt)
lastVS=$(<lastVS.txt)
lastAtk=$(<lastAtk.txt)
lastArena=$(<lastArena.txt)
lastWind=$(<lastWind.txt)
lastResult=$(<lastResult.txt)
lastBold=$(<lastBold.txt)
lastStrike=$(<lastStrike.txt)
# create a textfile called "tele" with your script name for sending to $telegram
telegram=$(<tele)
allowedQuests=0
allowedDice=0
inBattle=0
isAttacking=0
inForest=0
pause=10
[ -n "$lastLevelUp" ] && delta=0 || delta=2
baris=$((2+$delta))
nama=$(grep -a -A $baris ^$lastStaminaStat $log | tail -1)
nama=$(echo "${nama%\ of*}" | rev | cut -d" " -f2- | rev)
echo "$nama"
baris=$((3+$delta))
level=$(grep -a -A $baris ^$lastStaminaStat $log | tail -1 | cut -d" " -f2)
baris=$((4+$delta))
atk=$(grep -a -A $baris ^$lastStaminaStat $log | tail -1 | cut -d" " -f2)
def=$(grep -a -A $baris ^$lastStaminaStat $log | tail -1 | cut -d" " -f4)
baris=$((5+$delta))
exp=$(grep -a -A $baris ^$lastStaminaStat $log | tail -1 | cut -d" " -f2)
you="$nama won! - he takes 20💰"
baris=$((6+$delta))
stamina=$(grep -a -A $baris ^$lastStaminaStat $log | tail -1 | cut -d" " -f2 | cut -d"/" -f1)
baris=$((13+$delta))
state=$(grep -a -A $baris ^$lastStaminaStat $log | tail -1 | cut -d" " -f1)
if [ "$state" = "State:" ]; then
  baris=$((14+$delta))
  state=$(grep -a -A $baris ^$lastStaminaStat $log | tail -1 | cut -d" " -f1-2)
fi
baris=$((11+$delta))
bag=$(grep -a -A $baris ^$lastStaminaStat $log | tail -1 | cut -d" " -f2 | cut -d"/" -f1)
baglimit=$(grep -a -A $baris ^$lastStaminaStat $log | tail -1 | cut -d" " -f2 | cut -d"/" -f2)
baris=$((7+$delta))
mana=$(grep -a -A $baris ^$lastStaminaStat $log | tail -1 | cut -d" " -f2)
baris=$((8+$delta))
gold=$(grep -a -A $baris ^$lastStaminaStat $log | tail -1 | cut -d" " -f1)
gold=${gold#?}
if [ -z "$gold" ]; then
  baris=$((7+$delta))
  gold=$(grep -a -A $baris ^$lastStaminaStat $log | tail -1 | cut -d" " -f1)
  gold=${gold#?}
  mana=0
  baris=$((10+$delta))
  bag=$(grep -a -A $baris ^$lastStaminaStat $log | tail -1 | cut -d" " -f2 | cut -d"/" -f1)
  baglimit=$(grep -a -A $baris ^$lastStaminaStat $log | tail -1 | cut -d" " -f2 | cut -d"/" -f2)
fi
nextForest=0
jam=`date '+ %H:%M'`
baris=$((6+$delta))
nextStamina=$(grep -a -A $baris ^$lastStaminaStat $log | tail -1 | cut -d" " -f3)
nextStamina=${nextStamina//⏰/}
nextStamina=${nextStamina//min/}
nextStamina=${nextStamina//now/0}
nextStamina=$((nextStamina+1))
nextStamina=$(dateutils.dadd `date '+ %H:%M'` +${nextStamina}m -f %H:%M)
minutes=$(grep -a -A $delta ^$lastStaminaStat $log | tail -1 | rev | cut -d" " -f2 | rev)
if [ "$minutes" != "few" ]; then 
  hour=$(grep -a -A $delta ^$lastStaminaStat $log | tail -1 | rev | cut -d" " -f3 | rev)
  echo $hour | grep -q h && hour=${hour//h/"*60"} && hour=$((hour)) || h=0
  minutes=$((hour+minutes+5))
else
  minutes=5
fi    
nextReport=$(dateutils.dadd `date '+ %H:%M'` +${minutes}m -f %H:%M)
echo "init"
noQuest=0
$telegram msg $cwBot "🏅Me" &
sleep 2

while true; do
  jam=`date +%k%M`
  if [ $jam -eq 2142 ] || [ $jam -eq 542 ] || [ $jam -eq 1342 ]; then
     "$telegram" msg "$cwBot /t_01" &
     sleep 3
     threadPrice=$(grep -a -A 1 "user#408101137 ... Thread offers now:" "$log" | grep -v fwd | tail -1 | rev | cut -d" " -f1 | rev)
     threadPrice=${threadPrice%?}
     echo "thread $threadPrice $gold $amount"
     if [ $gold -gt 0 ]; then
       amount=$(( $gold / $threadPrice ))
       if [ $amount -gt 0 ]; then
         "$telegram" msg "$cwBot /wtb_01_$amount" &
         sleep 3
       fi
     fi
     "$telegram" msg "$cwBot /hide" &
     hiding=1
  else
     hiding=0
  fi
  if [ $jam -eq 2135 ] || [ $jam -eq 535 ] || [ $jam -eq 1335 ]; then
    noQuest=1
  fi
  if [ $jam -eq 2200 ] || [ $jam -eq 600 ] || [ $jam -eq 1400 ]; then
    noQuest=0
  fi
  if [ $jam -eq 2210 ] || [ $jam -eq 610 ] || [ $jam -eq 1410 ]; then
    tunggu=3
    "$telegram" msg "$cwBot ⚖Exchange" &
    sleep $tunggu
    curExchange=$(grep -a "user#408101137 ... Here you can buy and sell some items." $log | grep -v fwd | tail -1 | cut -d" " -f1)
    tmpSlots=$(grep -a -A 6 "$curExchange" $log | tail -1)
    echo "$tmpSlots"
    Slots=$(echo "$tmpSlots" | cut -d"/" -f2- | cut -d")" -f1)
    Deals=$(echo "$tmpSlots" | cut -d"(" -f2- | cut -d"/" -f1)
    echo "deals $Deals of $Slots"
    Times=$(( 2*$Deals + 6 )) 
    echo "times $Times"
    for (( i=8; i<=$Times; i+=2 )) ; do
      cmd=$(grep -a -A $i "$curExchange" $log | tail -1 | cut -d"/" -f2-)
      "$telegram" msg "$cwBot /$cmd" &
      sleep $tunggu
    done
    sleep $tunggu
    "$telegram" msg "$cwBot ⚖Exchange" &
    sleep $tunggu
  fi
  if [ "$state" = "🛌Rest" ]; then
    #echo "rest"
    sleep 2
    $telegram msg $cwBot "🏅Me" &
    sleep 2
  fi
  curHide=$(grep -a "user#408101137 ... /hide" "$log" | grep -v fwd | tail -1 | cut -d" " -f1)
  if [ "$curHide" != "$lastHide" ]; then
    [ $hiding -eq 1 ] && tunggu=10 || tunggu=3
    echo "$curHide" > lastHide.txt
    lastHide=$(<lastHide.txt)
    "$telegram" msg "$cwBot ⚖Exchange" &
    sleep $tunggu
    curExchange=$(grep -a "user#408101137 ... Here you can buy and sell some items." $log | grep -v fwd | tail -1 | cut -d" " -f1)
    tmpSlots=$(grep -a -A 6 "$curExchange" $log | tail -1)
    echo "$tmpSlots"
    Slots=$(echo "$tmpSlots" | cut -d"/" -f2- | cut -d")" -f1)
    Deals=$(echo "$tmpSlots" | cut -d"(" -f2- | cut -d"/" -f1)
    echo "deals $Deals of $Slots"
    Times=$(( 2*$Deals + 6 )) 
    echo "times $Times"
    for (( i=8; i<=$Times; i+=2 )) ; do
      cmd=$(grep -a -A $i "$curExchange" $log | tail -1 | cut -d"/" -f2-)
      "$telegram" msg "$cwBot /$cmd" &
      sleep $tunggu
    done
    sleep $tunggu
    "$telegram" msg "$cwBot ⚖Exchange" &
    sleep $tunggu
    "$telegram" msg "$cwBot /stock" &
    sleep $tunggu
    curStock=$(grep -a "user#408101137 ... 📦Storage (" "$log" | grep -v fwd | tail -1 | cut -d" " -f1)
    jmlStock=$(grep -a "$curStock" "$log")
    totalStorage=$(echo "$jmlStock" | cut -d"/" -f2) 
    totalStorage="${totalStorage%??}"
    ocuStorage=$(echo "$jmlStock" | cut -d"/" -f1 | cut -d"(" -f2)
    echo "total $totalStorage kepake $ocuStorage"
    declare -a listStock
    declare -a nStock
    declare -i after; after=1
    echo ${listStock[*]}
    listStock[$after]=$(grep -a -A $after "$curStock" "$log" | tail -1)
    firstWord=$(echo "${listStock[$after]}" | cut -d" " -f1)
    number=$(echo "${listStock[$after]}" | rev | cut -d"(" -f1 | rev)
    nStock[$after]=${number%?}
    listStock[$after]=$(echo "${listStock[$after]}" | rev | cut -d"(" -f2- | rev)
    [ "$firstWord" = "User" ] && sizeWord=48 || sizeWord="${#firstWord}"
    while [ "$sizeWord" -ne 48 ]; do
      declare -i after; after+=1
      listStock[$after]=$(grep -a -A $after "$curStock" "$log" | tail -1)
      firstWord=$(echo "${listStock[$after]}" | cut -d" " -f1)
      number=$(echo "${listStock[$after]}" | rev | cut -d"(" -f1 | rev)
      nStock[$after]=${number%?}
      listStock[$after]=$(echo "${listStock[$after]}" | rev | cut -d"(" -f2- | rev)
      [ "$firstWord" = "User" ] && sizeWord=48 || sizeWord="${#firstWord}"
      [ "${listStock[$after]}" = "${listStock[$after-1]}" ] && sizeWord=48
      echo "name ${listStock[$after]} stock ${nStock[$after]}"
    done
    totalStock=$((after-1))
    echo "total stock $totalStock"
    echo "" > listPrice.txt
    for (( i=1; i<=$totalStock; i++ )); do
      if [ "${listStock[$i]}" = "Bone " ]; then
        "$telegram" msg "$cwBot /t_04" &
      elif [ "${listStock[$i]}" = "Coal " ]; then
        "$telegram" msg "$cwBot /t_05" &
      elif [ "${listStock[$i]}" = "Leather " ]; then
        "$telegram" msg "$cwBot /t_20" &
      elif [ "${listStock[$i]}" = "Powder " ]; then
        "$telegram" msg "$cwBot /t_07" &
      elif [ "${listStock[$i]}" = "Silver frame " ] || [ "${listStock[$i]}" = "Purified powder " ] || [ "${listStock[$i]}" = "Silver alloy " ] || [ "${listStock[$i]}" = "Silver mold " ] || [ "${listStock[$i]}" = "Steel mold " ] || [ "${listStock[$i]}" = "Cord " ] || [ "${listStock[$i]}" = "Artisan frame " ] || [ "${listStock[$i]}" = "Blacksmith frame " ] || [ "${listStock[$i]}" = "Wooden shaft " ]; then
	whatevs=0
      else
        "$telegram" msg "$cwBot /t ${listStock[$i]}" &
      fi
      sleep $tunggu
      if [ "${listStock[$i]}" != "Silver frame " ] && [ "${listStock[$i]}" != "Purified powder " ] && [ "${listStock[$i]}" != "Silver alloy " ] && [ "${listStock[$i]}" != "Silver mold " ] && [ "${listStock[$i]}" != "Steel mold " ] && [ "${listStock[$i]}" != "Cord " ] && [ "${listStock[$i]}" != "Artisan frame " ] && [ "${listStock[$i]}" != "Blacksmith frame " ] && [ "${listStock[$i]}" != "Wooden shaft " ]; then
        idxPrice=$(grep -a -A 1 "user#408101137 ... ${listStock[$i]}offers now:" "$log" | grep -v fwd | tail -1 | rev | cut -d" " -f1 | rev) || idxPrice="00"
        idxStock=$(grep -a -A 5 "user#408101137 ... ${listStock[$i]}offers now:" "$log" | grep -v fwd | tail -1 | rev | cut -d"_" -f1 | rev) || idxStock="00"
        echo "${idxPrice%?} ${nStock[$i]} ${listStock[$i]}$idxStock" >> listPrice.txt
      fi
    done
    sort -nr listPrice.txt | head -$Slots > sortedPrice.txt
    $telegram send_text $cwBot `realpath sortedPrice.txt` &
    while read i; do
      echo "baris i = $i"
      jumlah=$(echo "$i" | cut -d" " -f2)
      kode=$(echo "$i" | rev | cut -d" " -f1 | rev)
      echo "kode $kode"
      if [[ "01,02,03,04,05,06,07,09,13,20,21,22,23,24,31,35,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69" == *$kode* ]]; then
        batas=1000
      elif [[ "08,10,11,12,14,15,16,17,18,19,27,28,32,33,34,501,502" == *$kode* ]]; then
	batas=500
      elif [[ "25,29,30" == *$kode* ]]; then
	batas=333
      fi
      echo "batas $batas"
      if [ $jumlah -gt $batas ]; then
	deposit=$(($jumlah-$batas))
	jumlah=$batas
        "$telegram" msg "$cwBot" /g_deposit "$kode $deposit" &
	sleep $tunggu
      fi
      "$telegram" msg "$cwBot" /wts_"$kode"_"$jumlah"_1000 &
      sleep $tunggu
    done < sortedPrice.txt
    hiding=0
  fi
  curStrike=$(grep -a "user#408101137 ... You are ready to strike. The next battle is in" $log | grep -v fwd | tail -1 | cut -d" " -f1)
  if [ "$curStrike" != "$lastStrike" ]; then
    echo "$curStrike" > lastStrike.txt
    lastStrike=$(<lastStrike.txt)
    "$telegram" msg "$cwBot" "🏅Me" &
    sleep 2
  fi
  curBold=$(grep -a "user#408101137 ... Ha, bold enough? Choose an enemy!" $log | grep -v fwd | tail -1 | cut -d" " -f1)
  if [ "$curBold" != "$lastBold" ]; then
    echo $curBold > lastBold.txt
    lastBold=$(<lastBold.txt)
    if [ $isAttacking -eq 1 ]; then
      $telegram msg $cwBot "$castle" &
    fi
  fi
  curWind=$(grep -a "user#408101137 ... The wind is howling in the meadows, castles are unusually quiet. Warriors are mending their wounds and repairing armor after a tiresome battle. All the establishments and castle gates are closed for the next couple of minutes. Await the battle report at @chtwrsReports" $log | grep -v fwd | tail -1 | cut -d" " -f1)
  if [ "$curWind" != "$lastWind" ]; then
    echo $curWind > lastWind.txt
    lastWind=$(<lastWind.txt)
    inBattle=1
    sleep 60
  fi
  curResult=$(grep -a -B 1 "^Your result on the battlefield:" $log | grep -a "user#408101137 ... " | grep -v fwd | tail -1 | cut -d" " -f1)
  if [ "$curResult" != "$lastResult" ]; then
    echo $curResult > lastResult.txt
    lastResult=$(<lastResult.txt)
    inBattle=0
    isAttacking=0
  fi  
  curVS=$(grep -a -B 1 ^VS $log | grep -a "user#408101137 ... " | grep -v fwd | tail -1 | cut -d" " -f1)
  if [ "$curVS" != "$lastVS" ]; then
    echo $curVS > lastVS.txt
    lastVS=$(<lastVS.txt)
    p1=$(grep -a ^$curVS $log | cut -d" " -f6)
    p1=${p1%❤️*}
    p2=$(grep -a -A 2 ^$curVS $log | tail -1)
    p2=${p2%❤️*}
    if [ $p1 -le 0 ] || [ $p2 -le 0 ]; then
      #echo "VS"
      $telegram msg $cwBot "🏅Me" &
      sleep 2
      $telegram msg $cwBot "📯Arena" &
      pause=10
    else
      pause=2
      rand=$(($RANDOM % 3))
      $telegram msg $cwBot "${serang[$rand]}" &
    fi
  fi
  curArena=$(grep -a "user#408101137 ... 📯Welcome to Arena!" $log | grep -v fwd | tail -1 | cut -d" " -f1)
  if [ "$curArena" != "$lastArena" ]; then
    echo $curArena > lastArena.txt
    lastArena=$(<lastArena.txt)
    fight=$(grep -a -A 4 ^$curArena $log | tail -1 | cut -d" " -f3 | cut -d"/" -f1)
    if [ $gold -ge 5 ] && [ $fight -lt 5 ]; then 
      pause=2
      $telegram msg $cwBot "🔎Fight!" &
    else
      pause=10
      #echo "Arena"
      $telegram msg $cwBot "🏅Me" &
      sleep 2
    fi
  fi
  curAtk=$(grep -a "user#408101137 ... Great! Now decide upon your attack!" $log | grep -v fwd | tail -1 | cut -d" " -f1)
  if [ "$curAtk" != "$lastAtk" ]; then
    echo $curAtk > lastAtk.txt
    lastAtk=$(<lastAtk.txt)
    rand=$(($RANDOM % 3))
    $telegram msg $cwBot "${serang[$rand]}" &
    pause=2
  fi  
  curFight=$(grep -a "user#408101137 ... The fight begins! You are challenged by" $log | grep -v fwd | tail -1 | cut -d" " -f1)
  if [ "$curFight" != "$lastFight" ]; then
    echo $curFight > lastFight.txt
    lastFight=$(<lastFight.txt)
    rand=$(($RANDOM % 3))
    $telegram msg $cwBot "${serang[$rand]}" &
    pause=2
  fi
  curDef=$(grep -a "user#408101137 ... Great! Now select your defence!" $log | grep -v fwd | tail -1 | cut -d" " -f1)
  if [ "$curDef" != "$lastDef" ]; then
    echo $curDef > lastDef.txt
    lastDef=$(<lastDef.txt)
    rand=$(($RANDOM % 3))
    sleep 2
    $telegram msg $cwBot "${defend[$rand]}" &
    pause=2   
  fi
  curDice=$(grep -a "user#408101137 ... 🎲You threw the dice on the table:" $log | grep -v fwd | tail -1 | cut -d" " -f1)
  if [ "$curDice" != "$lastDice" ]; then
    inBattle=0
    echo $curDice > lastDice.txt
    lastDice=$(<lastDice.txt)
    won=$(grep -a -A 5 ^$curDice $log | tail -1)
    echo "$won"
    $telegram msg $cwBot "🏅Me" &
    sleep 2    
    if [ "$won" = "$you" ]; then     
      $telegram msg $cwBot "🎲Play some dice" &
      sleep 2
    elif [ "$isAttacking" -eq 1 ]; then
      echo "castle $castle" 
      $telegram msg $cwbot "$castle" &
    fi
  fi
  curDefeated=$(grep -a "user#408101137 ... You successfully defeated " $log | tail -1 | cut -d" " -f1)
  if [ "$curDefeated" != "$lastDefeated" ]; then
    inBattle=0
    echo $curDefeated > lastDefeated.txt
    lastDefeated=$(<lastDefeated.txt)
    #echo "defeated"
    $telegram msg $cwBot "🏅Me" &
    sleep 2
  fi
  curBeat=$(grep -a "noticed you and nearly beat you to death. You crawled back home to a nice warm bath. On your way there you dropped a couple of coins. Lost:" $log | grep -v fwd | grep -a "user#408101137 ... " | tail -1 | cut -d" " -f1)
  if [ "$curBeat" != "$lastBeat" ]; then
    inBattle=0
    echo $curBeat > lastBeat.txt
    lastBeat=$(<lastBeat.txt)
    #echo "beat"
    $telegram msg $cwBot "🏅Me" &
    sleep 2
  fi
  curDire=$(grep -a "user#408101137 ... In a dire need for an adventure, you went to a forest. You'll be back in " $log | tail -1 | cut -d" " -f1)
  if [ "$curDire" != "$lastDire" ]; then
    echo $curDire > lastDire.txt
    lastDire=$(<lastDire.txt)
    minutes=$(grep -a $curDire $log | tail -1 | rev | cut -d" " -f2 | rev)
    minutes=$((minutes+1))
    at now +$minutes minutes -f me
    tnext=$(dateutils.dadd `date '+ %H:%M'` +${minutes}m -f %H:%M)
  fi
  #curSwamp=$(grep -a "user#408101137 ... An adventure is calling. But you went to a swamp. You'll be back in " $log | tail -1 | cut -d" " -f1)
  curSwamp=$(grep -a "user#408101137 ... You went to the Dead Marshes. The dead are near. Back in " $log | tail -1 | cut -d" " -f1)
  if [ "$curSwamp" != "$lastSwamp" ]; then
    echo $curSwamp > lastSwamp.txt
    lastSwamp=$(<lastSwamp.txt)
    minutes=$(grep -a $curSwamp $log | tail -1 | rev | cut -d" " -f2 | rev)
    minutes=$((minutes+1))
    at now +$minutes minutes -f me
    tnext=$(dateutils.dadd `date '+ %H:%M'` +${minutes}m -f %H:%M)
    allowedQuests=1
  fi
  curValley=$(grep -a "user#408101137 ... You went to the Devil's Valley. Don't lose your soul. Back in " $log | tail -1 | cut -d" " -f1)
  if [ "$curValley" != "$lastValley" ]; then
    echo $curValley > lastValley.txt
    lastValley=$(<lastValley.txt)
    minutes=$(grep -a $curValley $log | tail -1 | rev | cut -d" " -f2 | rev)
    minutes=$((minutes+1))
    at now +$minutes minutes -f me
    tnext=$(dateutils.dadd `date '+ %H:%M'` +${minutes}m -f %H:%M)
    allowedQuests=1
  fi
  #curMountain=$(grep -a "user#408101137 ... Mountains can be a dangerous place. You decided to investigate, what's going on. You'll be back in " $log | tail -1 | cut -d" " -f1)
  curMountain=$(grep -a "user#408101137 ... You went to the Mirkwood. Beware of spooders. Back in " $log | tail -1 | cut -d" " -f1)
  if [ "$curMountain" != "$lastMountain" ]; then
    echo $curMountain > lastMountain.txt
    lastMountain=$(<lastMountain.txt)
    minutes=$(grep -a $curMountain $log | tail -1 | rev | cut -d" " -f2 | rev)
    minutes=$((minutes+1))
    at now +$minutes minutes -f me
    tnext=$(dateutils.dadd `date '+ %H:%M'` +${minutes}m -f %H:%M)
    allowedQuests=1
  fi
  curBattle=$(grep -a "user#408101137 ... Battle is coming. You have no time for games." $log | tail -1 | cut -d" " -f1)
  if [ "$curBattle" != "$lastBattle" ]; then
    echo $curBattle > lastBattle.txt
    inBattle=1
  fi
  curStaminaRestored=$(grep -a "user#408101137 ... Stamina restored. You are ready for more adventures!" $log | tail -1 | cut -d" " -f1)
  if [ "$curStaminaRestored" != "$lastStaminaRestored" ]; then
    echo $curStaminaRestored > lastStaminaRestored.txt
    lastStaminaRestored=$(<lastStaminaRestored.txt)
    allowedQuests=1
    rand=$(($RANDOM % 4))
    $telegram msg $cwBot "${quest[$rand]}" &
    sleep 2
  fi
  curStop=$(grep -a "user#408101137 ... You tried stopping " $log | tail -1 | cut -d" " -f1)
  if [ "$curStop" != "$lastStop" ]; then
    inBattle=0
    echo $curStop > lastStop.txt
    lastStop=$(<lastStop.txt)
    $telegram msg $cwBot "🏅Me" &
    sleep 2
  fi
  curPillage=$(grep -a "was completely clueless. Village was successfully pillaged. You feel pleased about yourself." $log | grep -v fwd | grep -a "user#408101137 ..." | tail -1 | cut -d" " -f1)
  if [ "$curPillage" != "$lastPillage" ]; then
    inBattle=0
    echo $curPillage > lastPillage.txt
    lastPillage=$(<lastPillage.txt)
    $telegram msg $cwBot "🏅Me" &
    sleep 2
  fi
  curWin=$(grep -a "tried stopping you, but you were stronger. You have satisfied your lust for violence and left back home. Received:" $log | grep -v fwd | grep -a "user#408101137 ..." | tail -1 | cut -d" " -f1)
  if [ "$curWin" != "$lastWin" ]; then
    inBattle=0
    echo $curWin > lastWin.txt
    lastWin=$(<lastWin.txt)
    #echo "tried"
    $telegram msg $cwBot "🏅Me" &
    sleep 2
  fi
  #echo "level up original"
  curLevelUp=$(grep -a -B 1 "Press /level_up" $log | grep -v fwd | grep -a "user#408101137 ..." | tail -1 | cut -d" " -f1)
  if [ "$curLevelUp" != "$lastLevelUp" ]; then
    echo $curLevelUp > lastLevelUp.txt
    lastLevelUp=$(<lastLevelUp.txt)
#    $telegram msg $cwBot "/level_up" &
    attack=$(grep -a -A 7 ^$curLevelUp $log | tail -1 | cut -d" " -f2 | cut -d"+" -f1)
    defense=$(grep -a -A 7 ^$curLevelUp $log | tail -1 | cut -d" " -f4 | cut -d"+" -f1)
    sleep 2
    if [ $attack -gt 10 ] && [ $defense -lt 10 ]; then 
       sleep 1
#      $telegram msg $cwBot "+1 🛡Defence" &
#    else
#      $telegram msg $cwBot "+1 ⚔Attack" &
#      $telegram msg $cwBot "+1 🛡Defence" &
    fi
    sleep 2
    #echo "level"
    $telegram msg $cwBot "🏅Me" &
    sleep 2
  fi
  [ -n "$lastLevelUp" ] && delta=0 || delta=2
  #echo "delta $delta"
  curStaminaStat=$(grep -a -B $delta "Battle of the seven castles" $log | grep -a "user#408101137 ..." | tail -1 | cut -d" " -f1)
  if [ "$curStaminaStat" != "$lastStaminaStat" ]; then
    echo $curStaminaStat > lastStaminaStat.txt
    lastStaminaStat=$(<lastStaminaStat.txt)
    minutes=$(grep -a -B $delta ^$curStaminaStat $log | tail -1 | rev | cut -d" " -f2 | rev)
    if [ "$minutes" != "few" ]; then 
      hour=$(grep -a -A $delta ^$curStaminaStat $log | tail -1 | rev | cut -d" " -f3 | rev)
      echo $hour | grep -q h && hour=${hour//h/"*60"} && hour=$((hour)) || h=0
      minutes=$((hour+minutes+5))
    else
      minutes=5
    fi    
    tnext=$(dateutils.dadd `date '+ %H:%M'` +${minutes}m -f %H:%M)
    if [ "$tnext" != "$nextReport" ]; then
      nextReport="$tnext"
    fi
    baris=$((6+$delta))    
    stamina=$(grep -a -A $baris ^$curStaminaStat $log | tail -1 | cut -d" " -f2 | cut -d"/" -f1)
    totalstamina=$(grep -a -A $baris ^$curStaminaStat $log | tail -1 | cut -d" " -f2 | cut -d"/" -f2)
    baris=$((2+$delta))
    nama=$(grep -a -A $baris ^$curStaminaStat $log | tail -1)
    nama=$(echo "${nama%\ of*}" | rev | cut -d" " -f2- | rev)
    you="$nama won! - he takes 20💰"
    baris=$((3+$delta))
    level=$(grep -a -A $baris ^$curStaminaStat $log | tail -1 | cut -d" " -f2)
    baris=$((4+$delta))
    atk=$(grep -a -A $baris ^$curStaminaStat $log | tail -1 | cut -d" " -f2)
    def=$(grep -a -A $baris ^$curStaminaStat $log | tail -1 | cut -d" " -f4)
    baris=$((5+$delta))
    exp=$(grep -a -A $baris ^$curStaminaStat $log | tail -1 | cut -d" " -f2)
    echo stamina $stamina $totalstamina
    if [ $stamina -eq $totalstamina ]; then
      allowedQuests=1
      sleep 2
      rand=$(($RANDOM % 4))
      $telegram msg $cwBot "${quest[$rand]}" &
      sleep 2
    fi
    baris=$((13+$delta))
    state=$(grep -a -A $baris ^$curStaminaStat $log | tail -1)
    if [ "$state" = "State:" ]; then
      baris=$((14+$delta))
      state=$(grep -a -A $baris ^$lastStaminaStat $log | tail -1 | cut -d" " -f1-2)
    fi
    baris=$((11+$delta))
    bag=$(grep -a -A $baris ^$curStaminaStat $log | tail -1 | cut -d" " -f2 | cut -d"/" -f1)
    baglimit=$(grep -a -A $baris ^$curStaminaStat $log | tail -1 | cut -d" " -f2 | cut -d"/" -f2)
    baris=$((7+$delta))
    mana=$(grep -a -A $baris ^$curStaminaStat $log | tail -1 | cut -d" " -f2)
    baris=$((8+$delta))
    gold=$(grep -a -A $baris ^$curStaminaStat $log | tail -1 | cut -d" " -f1)
    gold=${gold#?}
    if [ -z "$gold" ]; then
      baris=$((7+$delta))
      gold=$(grep -a -A $baris ^$curStaminaStat $log | tail -1 | cut -d" " -f1)
      gold=${gold#?}
      mana=0
      baris=$((10+$delta))
      bag=$(grep -a -A $baris ^$lastStaminaStat $log | tail -1 | cut -d" " -f2 | cut -d"/" -f1)
      baglimit=$(grep -a -A $baris ^$lastStaminaStat $log | tail -1 | cut -d" " -f2 | cut -d"/" -f2)
    fi
    baris=$((6+$delta))
    minutes=$(grep -a -A $baris ^$curStaminaStat $log | tail -1 | cut -d" " -f3)
    minutes=${minutes//⏰/}
    minutes=${minutes//min/}
    minutes=${minutes//now/0}
    minutes=$((minutes+1))
    tnext=$(dateutils.dadd `date '+ %H:%M'` +${minutes}m -f %H:%M)
    if [ "$tnext" != "$nextStamina" ]; then
      nextStamina="$tnext"
    fi
    if [ "$state" = "🛌Rest" ]; then
      if [ $allowedQuests -eq 1 ] && [ $noQuest -eq 0 ]; then
        if [ $stamina -gt 0 ]; then
          rand=$(($RANDOM % 4))
          $telegram msg $cwBot "${quest[$rand]}" &
          inForest=1
        else
          allowedQuests=0
          $telegram msg $cwBot "🛡Defend" &
        fi
      elif [ $isAttacking -eq 1 ]; then
        $telegram msg $cwBot "⚔Attack" &
      else
        $telegram msg $cwBot "🛡Defend" &
      fi
      sleep 2
    fi
    echo "check if attacking $state"
    if echo "$state" | grep Attacking; then
      isAttacking=1
      castle=$(echo "$state" | cut -d" " -f2)
      castle=${castle:0:1}
    fi
    echo "is attacking $isAttacking $castle"
  fi   
  #echo "level up skipping password"
  curLevelUp2=$(grep -a -B 3 "Press /level_up" $log | grep -v fwd | grep -a "user#408101137 ..." | tail -1 | cut -d" " -f1)
  if [ "$curLevelUp2" != "$lastLevelUp2" ]; then
    echo $curLevelUp2 > lastLevelUp2.txt
    lastLevelUp2=$(<lastLevelUp2.txt)
#    $telegram msg $cwBot "/level_up" &
    attack=$(grep -a -A 9 ^$curLevelUp2 $log | tail -1 | cut -d" " -f2 | cut -d"+" -f1)
    defense=$(grep -a -A 9 ^$curLevelUp2 $log | tail -1 | cut -d" " -f4 | cut -d"+" -f1)
    sleep 2
#    if [ $attack -gt 10 ] && [ $defense -lt 10 ]; then 
#      $telegram msg $cwBot "+1 🛡Defence" &
#    else
#      $telegram msg $cwBot "+1 ⚔Attack" &
#    fi
#    sleep 2
    #echo "level"
#    $telegram msg $cwBot "🏅Me" &
#    sleep 2
  fi
  #echo "going"
  curGo=$(grep -a "user#408101137 ... You were strolling around on your horse when you noticed" $log | grep -v fwd | tail -1 | cut -d" " -f1)
  while [ "$curGo" != "$lastGo" ]; do
    inBattle=0
    echo $curGo > lastGo.txt
    lastGo=$(<lastGo.txt)
    $telegram msg $cwBot "/go" &
    sleep 50
    curGo=$(grep -a "user#408101137 ... You were strolling around on your horse when you noticed" $log | grep -v fwd | tail -1 | cut -d" " -f1)
  done
  curEngage=$(grep -a "user#408101137 ... A wild " $log | grep -v fwd | tail -1 | cut -d" " -f1)
  while [ "$curEngage" != "$lastEngage" ]; do
    inBattle=0
    echo $curEngage > lastEngage.txt
    lastEngage=$(<lastEngage.txt)
    sleep 60
    $telegram msg $cwBot "/engage" &
    sleep 50
    curEngage=$(grep -a "user#408101137 ... A wild " $log | grep -v fwd | tail -1 | cut -d" " -f1)
  done
  t06=$((559-`date '+ %k%M'`))
  t14=$((1359-`date '+ %k%M'`))
  t22=$((2159-`date '+ %k%M'`))
  if [ $inBattle -eq 0 ] && [ $t06 -le 2 -a $t06 -gt 0 ] || [ $t14 -le 2 -a $t14 -gt 0 ] || [ $t22 -le 2 -a $t22 -gt 0 ]; then
    echo "inside selling logic"
    if [ $gold -ge 3100 ]; then 
      $telegram msg $cwBot "/buy_w07" &
    elif [ $gold -ge 2450 ]; then  
      $telegram msg $cwBot "/buy_w27" &
    elif [ $gold -ge 2424 ]; then  
      $telegram msg $cwBot "/buy_w22" &
    elif [ $gold -ge 2300 ]; then  
      $telegram msg $cwBot "/buy_w12" &
    elif [ $gold -ge 1800 ]; then  
      $telegram msg $cwBot "/buy_w06" &
    elif [ $gold -ge 1618 ]; then  
      $telegram msg $cwBot "/buy_w21" &
    elif [ $gold -ge 1470 ]; then  
      $telegram msg $cwBot "/buy_w26" &
    elif [ $gold -ge 1440 ]; then
      $telegram msg $cwBot "/buy_w05" &
    elif [ $gold -ge 1414 ]; then
      $telegram msg $cwBot "/buy_w11" &
    elif [ $gold -ge 976 ]; then
      $telegram msg $cwBot "/buy_w10" &
    elif [ $gold -ge 854 ]; then  
      $telegram msg $cwBot "/buy_w20" &
    elif [ $gold -ge 820 ]; then  
      $telegram msg $cwBot "/buy_w25" &
    elif [ $gold -ge 690 ]; then
      $telegram msg $cwBot "/buy_a05" &
    elif [ $gold -ge 645 ]; then
      $telegram msg $cwBot "/buy_w04" &
    elif [ $gold -ge 510 ]; then
      $telegram msg $cwBot "/buy_a10" &
    elif [ $gold -ge 498 ]; then
      $telegram msg $cwBot "/buy_w17" &
    elif [ $gold -ge 490 ]; then
      $telegram msg $cwBot "/buy_a15" &
    elif [ $gold -ge 430 ]; then
      $telegram msg $cwBot "/buy_a04" &
    elif [ $gold -ge 372 ]; then  
      $telegram msg $cwBot "/buy_w24" &
    elif [ $gold -ge 364 ]; then
      $telegram msg $cwBot "/buy_w09" &
    elif [ $gold -ge 311 ]; then
      $telegram msg $cwBot "/buy_w16" &
    elif [ $gold -ge 285 ]; then
      $telegram msg $cwBot "/buy_a19" &
    elif [ $gold -ge 250 ]; then
      $telegram msg $cwBot "/buy_a03" &
    elif [ $gold -ge 244 ]; then
      $telegram msg $cwBot "/buy_w03" &
    elif [ $gold -ge 238 ]; then
      $telegram msg $cwBot "/buy_a09" &
    elif [ $gold -ge 103 ]; then
      $telegram msg $cwBot "/buy_w15" &
    elif [ $gold -ge 92 ]; then
      $telegram msg $cwBot "/buy_a13" &
    elif [ $gold -ge 80 ]; then 
      $telegram msg $cwBot "/buy_a08" &
    elif [ $gold -ge 68 ]; then  
      $telegram msg $cwBot "/buy_w18" &
    elif [ $gold -ge 62 ]; then  
      $telegram msg $cwBot "/buy_w23" &
    elif [ $gold -ge 55 ]; then
      $telegram msg $cwBot "/buy_w08" &
    elif [ $gold -ge 50 ]; then
      $telegram msg $cwBot "/buy_w14" &
    elif [ $gold -ge 43 ]; then
      $telegram msg $cwBot "/buy_a17" &
    elif [ $gold -ge 35 ]; then
      $telegram msg $cwBot "/buy_a07" &
    elif [ $gold -ge 30 ]; then
      $telegram msg $cwBot "/buy_w02" &
    fi    
    sleep 2
    $telegram msg $cwBot "🏅Me" &
 #   [ $isAttacking -eq 0 ] && $telegram msg $cwBot "🛡Defend" &
    sleep 2
  fi
  #echo "checking date"
  tgl=$(date)
  tnow=$(date +%H:%M)
  echo "now $tnow stamina $nextStamina report $nextReport"
  mStamina=$(dateutils.ddiff -f "%M" $tnow $nextStamina)
  mReport=$(dateutils.ddiff -f "%M" $tnow $nextReport)
  #echo "checking in forest"
#  if [ $inForest -eq 1 ]; then
#    echo "forest $nextForest"
#    mForest=$(dateutils.ddiff -f "%M" $tnow $nextForest)
#  fi    
  if [ $mStamina -lt -10 ]; then
    tnow=$(date "+%F %H:%M")
    nStamina=$(date --date="next day" +%F)
    nStamina="$nStamina $nextStamina"
    echo "now $tnow nstamina $nStamina"
    mStamina=$(dateutils.ddiff -f "%M" "$tnow" "$nStamina")
  fi  
  if [ $mReport -lt -10 ]; then
    tnow=$(date "+%F %H:%M")
    nReport=$(date --date="next day" +%F)
    nReport="$nReport $nextReport"
    echo "now $tnow nreport $nReport"
    mReport=$(dateutils.ddiff -f "%M" "$tnow" "$nReport")
  fi  
  if [ $inBattle -eq 0 ] && [ $mStamina -eq 0 ] && [ $mReport -gt 5 ]; then
    #echo "stamina"
    $telegram msg $cwBot "🏅Me" &
    sleep 2
    if [ $gold -ge 10 ]; then
      $telegram msg $cwBot "🎲Play some dice" &
    fi
  fi
  if [ $mReport -lt 0 ]; then
    $telegram msg $cwBot "/report" &
    sleep 2
    if [ $bag -eq $baglimit ]; then
      $telegram msg $cwBot "💰Sell" &
      sleep 2
      curLine=$(grep -a "Items for sale:" $log | grep -v fwd | tail -1 | cut -d" " -f1)
      for i in `sed -n "/^$curLine/,/total sum:/p" $log | grep sell_ | cut -d"/" -f2`; do 
        j=$(grep -a "💰/$i" $log | tail -1 | cut -d" " -f1)
        for k in `seq 1 $j`;
        do 
          $telegram msg $cwBot "/$i" &
          sleep 2
        done
      done     
    fi       
    if [ $gold -ge 5 ]; then
      $telegram msg $cwBot "🔎Fight!" &
    fi
    sleep 2
    #echo "report"
    $telegram msg $cwBot "🏅Me" & 
    sleep 2
  fi 
  if [ $mStamina -gt 60 ]; then 
    $telegram msg $cwBot "🏅Me" & 
  fi
  tempStatus=$(echo "$nama $state, $level🏅, $atk⚔, $def🛡, $exp🔥, $stamina🔋, $mana💧, $gold💰, $bag🎒, next stamina: $mStamina minutes, next report: $mReport minutes")
  if [ "$status" != "$tempStatus" ]; then
    status="$tempStatus"
    echo "$tgl, $status"
    echo "attack $isAttacking $castle questing $allowedQuests no quest $noQuest" &
  fi
  sleep $pause  
done
