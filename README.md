Requirements

- telegram-cli, set up to write a raw logfile (it logs user id, not the user name) stripping the formatting
    example parameter for telegram-cli: --permanent-msg-ids -N -I -W -U username -G username -k TelegramKeyFile -L TelegramLog -P 1234 -d -f -vRC
- netcat to write on telegram-cli listening port (on above example is on port 1234)
- at command (some distros don't have this at command as the default installed package)
- dateutils package
- shell script wrapper to send telegram-cli commands, in this script is called "telegram.sh"
   send msg/photo/text etc. using Telegram message service => (https://github.com/vysheng/tg/blob/master/README.md#supported-commands)
   telegram.sh simply contains the netcat command: echo "$@" | nc 127.0.0.1 1234
